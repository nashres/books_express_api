/***
 * Nashres
 * This file is routing file for the books API
 */


const express = require('express');

const BookController = require("../controller/controller.js");
const router = express.Router();


//SHOW ROUTES

// Create a new Film
router.post("/books", BookController.createBook);
  
// Retrieve all Films
router.get("/books", BookController.getAllBooks);

// Retrieve a single Film with filmId
router.get("/books/:bookId", BookController.getBookById);

// Update a Film with filmId
router.put("/books/:bookId", BookController.UpdateBook);

//DELETE ROUTE
router.delete('/books/:bookId',BookController.deleteBook );



module.exports = router;