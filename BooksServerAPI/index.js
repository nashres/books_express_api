/***
 * Nashres
 * This file is the server of the API
 */

const express = require('express'),  
    backend = express(),  
    db = require("./db/connect.js"),
    router = require('./routes/route.js');

backend.use(express.json()); //to parse data from db to here as json object




//INDEX
backend.get('/', (req,res) => {
    res.redirect('/books');
});


//this will require adding /api/ before calling each router url
backend.use('/api', router);





backend.listen(3010, () => {
    console.log("server started successfully ...");
});

