const mysql = require("mysql");
const dbConfig = require("./db_config.js");

// connection string to connect to the database
/***
 * Nashres
 * This is the configuration file for MySQL database
 */

const connection = mysql.createConnection({
  host: dbConfig.HOST,
  user: dbConfig.USER,
  password: dbConfig.PASSWORD,
  database: dbConfig.DB
});

// connect to database
connection.connect(error => {
  if (error) throw error;
  console.log("Successfully connected to the database.");
});

module.exports = connection;