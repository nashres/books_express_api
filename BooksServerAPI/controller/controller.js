/***
 * Nashres
 * This file serves as a controller for the books API
 */


//importing the model to use it
const Book = require('../db/model/book-model');

//create a Book and save it to db using body json
createBook = (req, res) => {

    const body = req.body;
    //check if a Book was provided in the body
    if(!body) {
        return res.status(400).json({success:false, error: "You must provide a book"});
    }

    // Create a Book using constructor
    const Book = new Book(body);
    if(!Book) {
        return res.status(400).json({success:false, error: "Book creation failed."});
    }

    //call the create Book function from the model, check the errors and return result
    Book.create(Book, (err, BookResult) => {
        if (err)
          return res.status(400).json({error:err, message: "Book not created"});
        else 
          return res.status(200).json({success: true, id: BookResult.insertId, message: "Book created."}); 
      });
    
};

//update a Book in db using the Book id
UpdateBook = async (req, res) => {
    //get json object from request body
    const body = req.body;
    if(!body) {
        return res.status(400).json({success:false, error: "You must provide a book to update"});
    }
    const Book = new Book(body);

    //update the Book with the json data that was passed in the re.body
    //using the model function 'updateById', and passing as parameters
    //the id and the updated Book.
    Book.updateById(req.params.BookId, Book, (err, data) => {

        //if error was returned by the model
        if(err) {
            return res.status(400).json({success: false, error: err, message:"There was an error"});
        }
        //if no rows were affected
        else if (data.affectedRows == 0) {
              return res.status(404).json({success:false, message: "Book not found"});
        } 
        else //if everything went ok, return success.
              return res.status(200).json({success: true, message: "Book updated."});

    });
   
    
};


//delete a Book from db using ID
deleteBook = async (req, res) => {
    Book.findOneAndDelete(req.params.BookId, (err, data) => {
        //check if an error was returned from the model
        if(err) {
            return res.status(400).json({success:false, error: err,  message:"Book not deleted"});
        }
        //check if no rows were affected
        else if(data.affectedRows ==0 ) {
            return res.status(400).json({success: false, message:"Book was not found"});
        }
       
        else //if everything went ok return success
            return res.status(200).json({success:true, message:"Book was deleted successfully"});
        
    })
};

//get a Book from DB using ID
getBookById = async (req, res) => {
    console.log(req.params);
    //call the getOne method from the model to get a Book by ID
    Book.findOne(req.params.BookId, function(err, Book) {
        //check errors returned by the model and return result accordingly
        if(err) {
            return res.status(400).json({success: false, error: err});
        }
        if(!Book.length) {
            return res.status(404).json({success:false, error: "Book not found"});
        }
        return res.status(200).json({success:true, data: Book});
    });
};

//get all Books from the db
getAllBooks = async (req, res) =>{
    //call the getAll method from the model to get a list of all Books
    Book.getAll((err, Books) => {
        //check errors returned by the model and return result accordingly
        if(err) {
            return res.status(400).json({success: false, error: err});
        }
        if(!Books.length) {
            return res.status(404).json({success: false, error: "No Books found."});
        }
        return res.status(200).json({success: true, data: Books});
    });
};


//export all the methods for further use in other locations.
module.exports = {
    createBook,
    UpdateBook,
    deleteBook,
    getBookById,
    getAllBooks
}