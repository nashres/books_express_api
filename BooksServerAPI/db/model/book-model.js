/***
 * Nashres
 * This is the model of the API
 */

const sql = require("../connect.js");

// constructor
function book(book) {
  this.title = book.title;
  this.description = book.description;
  this.pagesNumber = book.pagesNumber;
};


//select all books in db and return the result
book.getAll = function(result) {
  sql.query("SELECT * FROM book", (err, res) => {
    result(err, res);
  });
};

//Find a book by its ID and return the result
book.findOne = (book_Id, result) => {
  sql.query(`SELECT * FROM book WHERE book_id = ${book_Id}`, (err, res) => {
    result(err, res);
    
  });
};

//Create new book  and return the result
book.create = (newbook, result) => {
  sql.query("INSERT INTO book SET ?", newbook, (err, res) => {
    result(err, res);
  });
};

//Update book using ID and return the result
book.updateById = (id, book, result) => {
  sql.query(
    "UPDATE book SET title = ?, description = ?, pagesNumber = ?",
    [book.title, book.description, book.pagesNumber],
    (err, res) => {
      result(err, res);
    }
  );
};


//Delete a book using ID and return the result


book.findOneAndDelete = (id, result) => {
  sql.query("DELETE FROM book WHERE book_id = ?", id, (err, res) => {
    console.log(err);
    console.log(res);
    result(err, res);
  });
};

//export the model book for further use in other locations.
module.exports = book;